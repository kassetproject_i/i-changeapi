﻿using kbank_I_Change_Api.DAL;
using kbank_I_Change_Api.Models.Login;
using kbank_I_Change_Api.Models.Shared;
using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.DirectoryServices.AccountManagement;
using kbank_I_Change_Api.SharedService;
using kbank_I_Change_Api.Models.DB;
using System.Security.Cryptography;

namespace kbank_I_Change_Api.BLL
{
    public class LoginService
    {
        private UserRepository _re_user;
        private JwtTokensService _se_jwtTokens;
        private dbService _dbService;
        public LoginService()
        {
            _dbService = new dbService();
            _re_user = new UserRepository(_dbService);
            _se_jwtTokens = new JwtTokensService();
        }

        public ResponseModel Login(UserLoginModel userLogin)
        {
            try
            {
                UserLoginResponseModel userModel;
                _dbService.OpenConnection();
                bool isPass = false;
                ICNG_M_SEC_USER userData = _re_user.GetUserLogin(userLogin);
                if (userData != null)
                {
                    switch (userData.AUTH_TYPE)
                    {
                        case "A":
                            isPass = CheckUserAd(userLogin);
                            break;
                        case "D":
                            isPass = CheckPassword(userLogin.Password, userData.PASSWORD);
                            break;
                        default:
                            isPass = false;
                            break;
                    }
                }

                ResponseModel res = new ResponseModel();
                if (isPass == true)
                {
                    ICNG_M_SEC_ROLE roleData = _re_user.GetUserRoleByRoleCode(userData.ROLE_CODE);
                    List<UserPermissionModel> permissionData = _re_user.GetPermissionByRoleCode(userData.ROLE_CODE);
                    List<string> pageCodes = permissionData.Select(p => p.PAGE_CODE).Distinct().ToList();
                    List<ICNG_M_SEC_PAGE> userMenuData = _re_user.GetMenuByPageCodes(pageCodes, userData);


                    userModel = new UserLoginResponseModel();
                    userModel.Username = userData.USER_NAME;
                    userModel.FirstName = userData.FIRST_NAME;
                    userModel.LastName = userData.LAST_NAME;
                    userModel.UserCode = userData.USER_CODE;
                    userModel.TokenKey = _se_jwtTokens.GenerateTokenLogin(userData, roleData, permissionData, userMenuData);
                    res.Successful = true;
                    res.Result = userModel;
                }
                else
                {
                    res.Successful = false;
                    res.Information = "ชื่อผู้ใช้งานหรือรหัสผ่านไม่ถูกต้อง";
                }

                return res;
            }
            finally
            {
                _dbService.CloseConnection();
            }
        }

        public ResponseModel Logout(UserDataModel userData)
        {
            _se_jwtTokens.SetTokenExpire(userData.Guid, userData.UserName);
            ResponseModel res = new ResponseModel();
            res.Successful = true;
            return res;
        }

        public ResponseModel GetCustomerMenu()
        {
            try
            {
                ResponseModel res = new ResponseModel();
                _dbService.OpenConnection();
                List<string> menuPageCodeCustomer = _re_user.GetPageCodeCustomerMenu();
                List<ICNG_M_SEC_PAGE> userMenuData = _re_user.GetMenuByPageCodes(menuPageCodeCustomer, null);
                res.Successful = true;
                res.Result = userMenuData;
                return res;
            }
            finally
            {
                _dbService.CloseConnection();
            }
        }

        private bool CheckUserAd(UserLoginModel userLogin)
        {
            string adString = WebConfigurationManager.AppSettings["adValue"];
            PrincipalContext principalContext = new PrincipalContext(ContextType.Domain, adString);
            bool flag = principalContext.ValidateCredentials(userLogin.Username, userLogin.Password);
            principalContext.Dispose();
            return flag;
        }

        private void GetUserAd(UserLoginModel userLogin,string username)
        {
            string adString = WebConfigurationManager.AppSettings["adValue"];
            UserAdModel userAdModel = null;
            PrincipalContext principalContext = new PrincipalContext(ContextType.Domain, adString);
            bool flag = principalContext.ValidateCredentials(userLogin.Username, userLogin.Password);
            principalContext.Dispose();


            DirectoryEntry entry = new DirectoryEntry("LDAP://" + adString, userLogin.Username, userLogin.Password);
            DirectorySearcher oSearcher = new DirectorySearcher(entry);
            oSearcher.Filter = string.Format("(sAMAccountName={0})", username);
            oSearcher.PropertiesToLoad.Add("cn");
            SearchResult oResult = oSearcher.FindOne();
            if (oResult != null)
            {
                DirectoryEntry user = oResult.GetDirectoryEntry();
                string firstName = user.Properties["givenName"].Value.ToString();
                string lastName = user.Properties["sn"].Value.ToString();
                string phoneNumber = user.Properties["telephoneNumber"].Value.ToString();
                string email = user.Properties["mail"].Value.ToString();

                userAdModel = new UserAdModel();
                userAdModel.FirstName = firstName;
                userAdModel.LastName = lastName;
                userAdModel.PhoneNumber = phoneNumber;
                userAdModel.Email = email;
            }
            else
            {

            }
        }

        #region Password Function
        private string CreatePassword(string password)
        {
            string salt = GenerateSalt();
            return GenerateHash(salt, password) + salt;
        }

        private string HashPassword()
        {
            const string chars = "ABCDEFGHJKLMNPQRSTUSWXYZabcdefghjklmnpqrstuvwxyz123456789*";
            var random = new Random();
            var result = new string(Enumerable.Repeat(chars, 6).Select(s => s[random.Next(s.Length)]).ToArray());
            return CreatePassword(result);
        }

        public bool CheckPassword(string plainTextPassword, string hashedPassword)
        {
            if (hashedPassword.Length != 60)
            {
                if (hashedPassword.Length == 32)
                    return hashedPassword == GenerateHashMd5(plainTextPassword);
                else
                    return false;
            }

            string hash = hashedPassword.Substring(0, 44);
            string salt = hashedPassword.Substring(44);
            string hashValue = GenerateHash(salt, plainTextPassword);


            return hash == hashValue;
        }

        private string GenerateHash(string salt, string password)
        {
            SHA256 sha256 = SHA256.Create();
            byte[] hashBytes = sha256.ComputeHash(System.Text.Encoding.Unicode.GetBytes(salt + password));
            return Convert.ToBase64String(hashBytes);
        }

        private string GenerateSalt()
        {
            byte[] saltBytes = new byte[12];
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            rng.GetBytes(saltBytes);
            return Convert.ToBase64String(saltBytes);
        }

        private string GenerateHashMd5(string password)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] dataMd5 = md5.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            System.Text.StringBuilder strBuilder = new System.Text.StringBuilder();

            for (int i = 0; i <= dataMd5.Length - 1; i++)
                strBuilder.Append(dataMd5[i].ToString("x2"));

            return strBuilder.ToString();
        }
        #endregion
    }
}