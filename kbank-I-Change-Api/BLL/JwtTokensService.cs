﻿using kbank_I_Change_Api.DAL;
using kbank_I_Change_Api.Models.DB;
using kbank_I_Change_Api.Models.Login;
using kbank_I_Change_Api.SharedService;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Web;

namespace kbank_I_Change_Api.BLL
{
    public class JwtTokensService
    {
        private string jwtIssuer = "KASSECT";
        private string jwtAudience = "ICHANGE";
        private string jwtKey = "ICHANGEKASSETTOYOU";
        private dbService _dbService;
        private AuthLoginRepository _re_authLogin;

        public JwtTokensService()
        {
            _dbService = new dbService();
            _re_authLogin = new AuthLoginRepository(_dbService);
        }

        public string GenerateTokenLogin(ICNG_M_SEC_USER userLogin, ICNG_M_SEC_ROLE roleData, List<UserPermissionModel> permissionData, List<ICNG_M_SEC_PAGE> userMenuData)
        {
            string NewGuid = Guid.NewGuid().ToString();
            List<Claim> claims = new List<Claim>
            {
                new Claim("UserID", userLogin.USER_ID.ToString()),
                new Claim("UserName", userLogin.USER_NAME),
                new Claim("FirstName", userLogin.FIRST_NAME),
                new Claim("LastName", userLogin.LAST_NAME),
                new Claim("UserCode", userLogin.USER_CODE),
                new Claim("Role", JsonConvert.SerializeObject(roleData)),
                new Claim("PermissionData", JsonConvert.SerializeObject(permissionData)),
                new Claim("UserMenuData", JsonConvert.SerializeObject(userMenuData)),
                new Claim(JwtRegisteredClaimNames.Jti, NewGuid)
            };

            SymmetricSecurityKey key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtKey));
            SigningCredentials creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            JwtSecurityToken token = new JwtSecurityToken(
                issuer: jwtIssuer,
                audience: jwtAudience,
                claims: claims,
                signingCredentials: creds
            );

            saveToken(userLogin, NewGuid);
            string res = new JwtSecurityTokenHandler().WriteToken(token);

            return res;
        }

        public bool ValidateToken(string authToken)
        {
            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            TokenValidationParameters validationParameters = GetValidationParameters(jwtKey);
            SecurityToken validatedToken;
            bool res = false;
            try
            {
                IPrincipal principal = tokenHandler.ValidateToken(authToken, validationParameters, out validatedToken);
                bool isNotExpireRes = checkTokenIsNotExpireAndRenew(authToken);
                if (isNotExpireRes)
                {
                    res = true;
                }
                else
                {
                    res = false;
                }
            }
            catch (Exception ex)
            {
                res = false;
            }
            return res;
        }

        public void SetTokenExpire(string guid, string actionUserid)
        {
            try
            {
                _dbService.OpenConnection();
                _re_authLogin.SetTokenExpire(guid, actionUserid);
            }
            finally
            {
                _dbService.CloseConnection();
            }
        }

        public JwtSecurityToken GetTokenData(string authToken)
        {
            JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
            SecurityToken jsonToken = handler.ReadToken(authToken);
            JwtSecurityToken tokenS = handler.ReadToken(authToken) as JwtSecurityToken;
            return tokenS;
        }

        private TokenValidationParameters GetValidationParameters(string jwtKeyValue)
        {
            return new TokenValidationParameters()
            {
                ValidateLifetime = false, // Because there is no expiration in the generated token
                ValidateAudience = false, // Because there is no audiance in the generated token
                ValidateIssuer = false,   // Because there is no issuer in the generated token
                ValidIssuer = jwtIssuer,
                ValidAudience = jwtAudience,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtKeyValue)) // The same key as the one that generate the token
            };
        }

        private void saveToken(ICNG_M_SEC_USER userLogin, string guid)
        {
            try
            {
                _dbService.OpenConnection();
                _re_authLogin.InsertTokenUser(userLogin, guid);
            }
            finally
            {
                _dbService.CloseConnection();
            }
        }

        private bool checkTokenIsNotExpireAndRenew(string authToken)
        {
            try
            {
                _dbService.OpenConnection();
                JwtSecurityToken res = GetTokenData(authToken);
                string guid = res.Claims.First(claim => claim.Type == "jti").Value;
                string actionUserid = res.Claims.First(claim => claim.Type == "UserName").Value;
                bool isNotExpireRes = _re_authLogin.checkTokenIsNotExpireAndRenew(guid, actionUserid);
                return isNotExpireRes;
            }
            finally
            {
                _dbService.CloseConnection();
            }
        }
    }
}