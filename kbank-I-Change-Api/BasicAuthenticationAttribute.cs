﻿using kbank_I_Change_Api.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace kbank_I_Change_Api
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false)]
    public class BasicAuthenticationAttribute : AuthorizationFilterAttribute
    {
        private JwtTokensService _se_jwtTokens;
        public BasicAuthenticationAttribute()
        {
            _se_jwtTokens = new JwtTokensService();
        }

        public override void OnAuthorization(HttpActionContext actionContext)
        {
            string authHeader = null;
            var auth = actionContext.Request.Headers.Authorization;
            bool validateRes = false;
            if (auth != null && auth.Scheme == "Bearer")
            {
                authHeader = auth.Parameter;
                validateRes = _se_jwtTokens.ValidateToken(authHeader);
            }

            if (!validateRes)
            {
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
            }

            base.OnAuthorization(actionContext);
        }
    }
}