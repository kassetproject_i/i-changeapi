﻿using kbank_I_Change_Api.Models.Shared;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using System.Web.Http.Results;

namespace kbank_I_Change_Api
{
    public class ExceptionUnhandledLogger : IExceptionHandler
    {
        public Task HandleAsync(ExceptionHandlerContext context, CancellationToken cancellationToken)
        {
            ResponseModel responseData = new ResponseModel();
            responseData.Successful = false;
            responseData.Information = context.Exception.Message;

            LogError(context, cancellationToken);

            //Necessary to return Json
            var jsonType = GlobalConfiguration.Configuration.Formatters.JsonFormatter;
            jsonType.SerializerSettings.Formatting = Newtonsoft.Json.Formatting.Indented;

            var response = context.Request.CreateResponse(HttpStatusCode.InternalServerError, responseData, jsonType);

            context.Result = new ResponseMessageResult(response);

            return Task.FromResult(0);
        }

        private void LogError(ExceptionHandlerContext context, CancellationToken cancellationToken)
        {
            string jsonContent = "";
            // Get request parameters JSON For POST/PUT/DELETE
            HttpContext.Current.Request.InputStream.Position = 0;
            if (HttpContext.Current.Request.HttpMethod.ToUpper() == "POST"
                || HttpContext.Current.Request.HttpMethod.ToUpper() == "PUT"
                || HttpContext.Current.Request.HttpMethod.ToUpper() == "DELETE")
            {
                using (var reader = new StreamReader(HttpContext.Current.Request.InputStream, System.Text.Encoding.UTF8, true, 4096, true))
                {
                    jsonContent = reader.ReadToEnd().ToString();
                }
                HttpContext.Current.Request.InputStream.Position = 0;
            }
        }
    }
}