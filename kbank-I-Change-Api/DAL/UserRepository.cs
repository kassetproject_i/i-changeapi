﻿using kbank_I_Change_Api.Models.DB;
using kbank_I_Change_Api.Models.Login;
using kbank_I_Change_Api.SharedService;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace kbank_I_Change_Api.DAL
{
    public class UserRepository
    {
        dbService _dbService;
        public UserRepository(dbService dbService)
        {
            _dbService = dbService;
        }

        public ICNG_M_SEC_USER GetUserLogin(UserLoginModel userLogin)
        {
            ICNG_M_SEC_USER userData = null;
            List<OracleParameter> oracleParameters = new List<OracleParameter>();
            oracleParameters.Add(new OracleParameter(":USERNAME", OracleDbType.Varchar2, userLogin.Username, ParameterDirection.Input));
            DataTable tableUserData = _dbService.ExecuteQueryString("SELECT * FROM ICNG_M_SEC_USER WHERE STATUS='Y' AND USER_NAME=:USERNAME", oracleParameters);
            if (tableUserData != null && tableUserData.Rows.Count > 0)
            {
                userData = new ICNG_M_SEC_USER();
                userData = tableUserData.Rows[0].ToObject<ICNG_M_SEC_USER>();
            }
            return userData;
        }

        public ICNG_M_SEC_ROLE GetUserRoleByRoleCode(string roleCode)
        {
            ICNG_M_SEC_ROLE roleData = null;
            List<OracleParameter> oracleParameters = new List<OracleParameter>();
            oracleParameters.Add(new OracleParameter(":ROLECODE", OracleDbType.Varchar2, roleCode, ParameterDirection.Input));
            DataTable tableRoleData = _dbService.ExecuteQueryString("SELECT * FROM ICNG_M_SEC_ROLE WHERE ROLE_CODE=:ROLECODE", oracleParameters);
            if (tableRoleData != null && tableRoleData.Rows.Count > 0)
            {
                roleData = new ICNG_M_SEC_ROLE();
                roleData = tableRoleData.Rows[0].ToObject<ICNG_M_SEC_ROLE>();
            }
            return roleData;
        }

        public List<string> GetPageCodeCustomerMenu()
        {
            List<string> menuPageCodeCustomer = new List<string>();
            menuPageCodeCustomer.Add("contactus");
            return menuPageCodeCustomer;
        }

        public List<UserPermissionModel> GetPermissionByRoleCode(string roleCode)
        {
            List<UserPermissionModel> userPermissionData = null;
            List<OracleParameter> oracleParameters = new List<OracleParameter>();
            oracleParameters.Add(new OracleParameter(":ROLECODE", OracleDbType.Varchar2, roleCode, ParameterDirection.Input));
            DataTable tableUserPermissionData = _dbService.ExecuteQueryString(@"SELECT
                                                                    a.role_page_id,
                                                                    a.role_page_code,
                                                                    a.role_code,
                                                                    a.page_code,
                                                                    b.permission,
                                                                    d.permission_name
                                                                    FROM 
                                                                    ICNG_M_SEC_ROLE_PAGE a
                                                                    INNER JOIN ICNG_M_SEC_ROLEPAGE_DETAIL b
                                                                    ON a.role_page_code = b.role_page_code
                                                                    INNER JOIN ICNG_M_SEC_PAGE c
                                                                    ON a.page_code = c.page_code
                                                                    INNER JOIN ICNG_M_SEC_PAGE_DETAIL d
                                                                    ON c.page_code = d.page_code AND b.permission = d.permission
                                                                    WHERE a.role_code=:ROLECODE", oracleParameters);
            if (tableUserPermissionData != null && tableUserPermissionData.Rows.Count > 0)
            {
                userPermissionData = new List<UserPermissionModel>();
                userPermissionData = tableUserPermissionData.DataTableToList<UserPermissionModel>();
            }
            return userPermissionData;
        }

        public List<ICNG_M_SEC_PAGE> GetMenuByPageCodes(List<string> pageCodes, ICNG_M_SEC_USER userData)
        {
            List<ICNG_M_SEC_PAGE> userMenuData = null;
            string pagecodeString = string.Join("','", pageCodes);
            string sqlString = @"SELECT * FROM ICNG_M_SEC_PAGE
                                WHERE STATUS='Y' AND PAGE_CODE IN('@PAGECODE')
                                union all
                                SELECT * FROM ICNG_M_SEC_PAGE
                                WHERE STATUS='Y' AND PAGE_CODE IN(SELECT PARENT_PAGE FROM ICNG_M_SEC_PAGE
                                WHERE STATUS='Y' AND PAGE_CODE IN('@PAGECODE'))";
            sqlString = sqlString.Replace("@PAGECODE", pagecodeString);
            DataTable tableUserMenuData = _dbService.ExecuteQueryString(sqlString, null);

            if (tableUserMenuData != null && tableUserMenuData.Rows.Count > 0)
            {
                userMenuData = new List<ICNG_M_SEC_PAGE>();
                userMenuData = tableUserMenuData.DataTableToList<ICNG_M_SEC_PAGE>();

                ICNG_M_SEC_PAGE selectDefaultPage = new ICNG_M_SEC_PAGE();
                if (userData != null && !string.IsNullOrEmpty(userData.ROLE_PAGE_CODE_DEFAULT))
                {
                    selectDefaultPage = userMenuData
                                                        .Where(p => p.PAGE_CODE == userData.ROLE_PAGE_CODE_DEFAULT)
                                                        .FirstOrDefault();
                }
                else
                {
                    selectDefaultPage = userMenuData.FirstOrDefault();
                    selectDefaultPage.PAGE_CODE = "welcome";
                    selectDefaultPage.URL = "welcome";
                }

                ICNG_M_SEC_PAGE selectDefaultPageA = selectDefaultPage.ToNewObj<ICNG_M_SEC_PAGE>();
                selectDefaultPageA.PAGE_LEVEL = "P";
                selectDefaultPageA.PAGE_NAME = "หน้าหลัก";
                selectDefaultPageA.PARENT_PAGE = "";
                selectDefaultPageA.SEQ_NO = -1;
                userMenuData.Add(selectDefaultPageA);
            }
            return userMenuData;
        }
    }
}