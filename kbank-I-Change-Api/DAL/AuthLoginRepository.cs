﻿using kbank_I_Change_Api.Models.DB;
using kbank_I_Change_Api.Models.Shared;
using kbank_I_Change_Api.SharedService;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;

namespace kbank_I_Change_Api.DAL
{
    public class AuthLoginRepository
    {
        dbService _dbService;
        DateTime dateNow;
        public AuthLoginRepository(dbService dbService)
        {
            _dbService = dbService;
            dateNow = DateTime.Now;
        }

        public void InsertTokenUser(ICNG_M_SEC_USER userLogin, string guid)
        {
            string IPAddress = GetIPAddress();

            List<OracleParameter> oracleParameters = new List<OracleParameter>();
            oracleParameters.Add(new OracleParameter(":UPDATEUSERID", OracleDbType.Varchar2, userLogin.USER_NAME, ParameterDirection.Input));
            oracleParameters.Add(new OracleParameter(":UPDATEDATETIME", OracleDbType.Date, dateNow, ParameterDirection.Input));
            oracleParameters.Add(new OracleParameter(":USERID", OracleDbType.Varchar2, userLogin.USER_ID, ParameterDirection.Input));
            int updateRes = _dbService.ExecuteNonQueryString("UPDATE ICNG_T_AUTH_LOGIN SET STATUS='E',UPDATE_USER_ID=:UPDATEUSERID,UPDATE_DATETIME=:UPDATEDATETIME WHERE STATUS='A' AND USER_ID=:USERID", oracleParameters);
            
            oracleParameters = new List<OracleParameter>();

            oracleParameters.Add(new OracleParameter(":TOKENID", OracleDbType.Varchar2, guid, ParameterDirection.Input));
            oracleParameters.Add(new OracleParameter(":USERID", OracleDbType.Varchar2, userLogin.USER_ID, ParameterDirection.Input));
            oracleParameters.Add(new OracleParameter(":CLIENTIP", OracleDbType.Varchar2, IPAddress, ParameterDirection.Input));
            oracleParameters.Add(new OracleParameter(":STATUS", OracleDbType.Varchar2, "A", ParameterDirection.Input));
            oracleParameters.Add(new OracleParameter(":CREATEUSERID", OracleDbType.Varchar2, userLogin.USER_NAME, ParameterDirection.Input));
            oracleParameters.Add(new OracleParameter(":CREATEDATETIME", OracleDbType.Date, dateNow, ParameterDirection.Input));

            int insertRes = _dbService.ExecuteNonQueryString(@"INSERT INTO ICNG_T_AUTH_LOGIN
                                                                (TOKEN_ID, USER_ID, CLIENT_IP, STATUS, CREATE_USER_ID, CREATE_DATETIME)
                                                                VALUES
                                                                (:TOKENID,:USERID,:CLIENTIP,:STATUS,:CREATEUSERID,:CREATEDATETIME)", oracleParameters);

            oracleParameters = new List<OracleParameter>();
            oracleParameters.Add(new OracleParameter(":USERID", OracleDbType.Varchar2, userLogin.USER_ID, ParameterDirection.Input));
            oracleParameters.Add(new OracleParameter(":TOKENID", OracleDbType.Varchar2, guid, ParameterDirection.Input));
            DataTable tableAuthloginData = _dbService.ExecuteQueryString("SELECT * FROM ICNG_T_AUTH_LOGIN WHERE STATUS='A' AND USER_ID=:USERID AND TOKEN_ID=:TOKENID", oracleParameters);
            ICNG_T_AUTH_LOGIN authloginData = null;
            if (tableAuthloginData != null && tableAuthloginData.Rows.Count > 0)
            {
                authloginData = new ICNG_T_AUTH_LOGIN();
                authloginData = tableAuthloginData.Rows[0].ToObject<ICNG_T_AUTH_LOGIN>();
            }

            if (authloginData != null)
            {
                InsertTokenExpire(authloginData.AUTH_LOGIN_ID, userLogin.USER_NAME);
            }

        }
        
        public void InsertTokenExpire(decimal authloginId,string actionUserid)
        {
            DateTime dateTimeNow = DateTime.Now;
            dateTimeNow = dateTimeNow.AddMinutes(30);

            List<OracleParameter> oracleParameters = new List<OracleParameter>();
            oracleParameters.Add(new OracleParameter(":AUTHLOGINID", OracleDbType.Decimal, authloginId, ParameterDirection.Input));
            oracleParameters.Add(new OracleParameter(":EXPIREDATE", OracleDbType.Date, dateTimeNow, ParameterDirection.Input));
            oracleParameters.Add(new OracleParameter(":CREATEUSERID", OracleDbType.Varchar2, actionUserid, ParameterDirection.Input));
            oracleParameters.Add(new OracleParameter(":CREATEDATETIME", OracleDbType.Date, dateNow, ParameterDirection.Input));
            int insertRes = _dbService.ExecuteNonQueryString(@"INSERT INTO ICNG_T_AUTH_LOGIN_DETAIL
                                                                (AUTH_LOGIN_ID,EXPIRE_DATE,CREATE_USER_ID,CREATE_DATETIME)
                                                                VALUES
                                                                (:AUTHLOGINID,:EXPIREDATE,:CREATEUSERID,:CREATEDATETIME)", oracleParameters);

        }

        public void SetTokenExpire(string guid, string actionUserid)
        {
            List<OracleParameter> oracleParameters = new List<OracleParameter>();
            oracleParameters.Add(new OracleParameter(":UPDATEUSERID", OracleDbType.Varchar2, actionUserid, ParameterDirection.Input));
            oracleParameters.Add(new OracleParameter(":UPDATEDATETIME", OracleDbType.Date, dateNow, ParameterDirection.Input));
            oracleParameters.Add(new OracleParameter(":TOKENID", OracleDbType.Varchar2, guid, ParameterDirection.Input));
            int updateRes = _dbService.ExecuteNonQueryString("UPDATE ICNG_T_AUTH_LOGIN SET STATUS='E',UPDATE_USER_ID=:UPDATEUSERID,UPDATE_DATETIME=:UPDATEDATETIME WHERE STATUS='A' AND TOKEN_ID=:TOKENID", oracleParameters);
        }

        public bool checkTokenIsNotExpireAndRenew(string guid, string actionUserid)
        {
            bool isNotExpireRes = true;
            string sql = @"SELECT a.*,b.auth_login_detail_id,b.expire_date FROM ICNG_T_AUTH_LOGIN a
                            INNER JOIN ICNG_T_AUTH_LOGIN_DETAIL b
                            ON a.auth_login_id = b.auth_login_id
                            WHERE a.token_id=:TOKENID AND a.status='A'
                            ORDER BY b.expire_date desc
                            fetch first 1 row only";

            List<OracleParameter> oracleParameters = new List<OracleParameter>();
            oracleParameters.Add(new OracleParameter(":TOKENID", OracleDbType.Varchar2, guid, ParameterDirection.Input));
            DataTable tableRes = _dbService.ExecuteQueryString(sql, oracleParameters);
            if (tableRes != null && tableRes.Rows.Count > 0)
            {
                AuthLoginCheckTokenModel authLoginCheckToken = new AuthLoginCheckTokenModel();
                authLoginCheckToken = tableRes.Rows[0].ToObject<AuthLoginCheckTokenModel>();
                TimeSpan diffTime = (DateTime.Now - authLoginCheckToken.EXPIRE_DATE);
                if (diffTime.Minutes > 0)
                {
                    isNotExpireRes = false;
                }
                else
                {
                    InsertTokenExpire(authLoginCheckToken.AUTH_LOGIN_ID, actionUserid);
                }
            }
            else
            {
                isNotExpireRes = false;
            }

            return isNotExpireRes;
        }

        private string GetIPAddress()
        {
            string IPAddress = "";
            IPHostEntry Host = default(IPHostEntry);
            string Hostname = null;
            Hostname = System.Environment.MachineName;
            Host = Dns.GetHostEntry(Hostname);
            foreach (IPAddress IP in Host.AddressList)
            {
                if (IP.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                {
                    IPAddress = Convert.ToString(IP);
                }
            }
            return IPAddress;
        }
    }
}