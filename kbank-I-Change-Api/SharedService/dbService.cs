﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace kbank_I_Change_Api.SharedService
{
    public class dbService
    {
        private string _dbConnectionString;
        private OracleConnection _con;
        public dbService()
        {
            _dbConnectionString = WebConfigurationManager.AppSettings["dbConnection"];
            _con = new OracleConnection(_dbConnectionString);
        }

        public void OpenConnection()
        {
            _con.Open();
        }

        public DataTable ExecuteQueryString(string sqlString, List<OracleParameter> oracleParameters)
        {
            OracleCommand sqlCommand = new OracleCommand(sqlString, _con);
            if (oracleParameters != null)
            {
                oracleParameters.ForEach(delegate (OracleParameter itemOracleParameter)
                {
                    sqlCommand.Parameters.Add(itemOracleParameter);
                });
            }
            OracleDataReader oracleDataReader = sqlCommand.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(oracleDataReader);

            return dataTable;
        }

        public int ExecuteNonQueryString(string sqlString, List<OracleParameter> oracleParameters)
        {
            OracleCommand sqlCommand = new OracleCommand(sqlString, _con);
            if (oracleParameters != null)
            {
                oracleParameters.ForEach(delegate (OracleParameter itemOracleParameter)
                {
                    sqlCommand.Parameters.Add(itemOracleParameter);
                });
            }
            int res = sqlCommand.ExecuteNonQuery();

            return res;
        }
        
        public void CloseConnection()
        {
            //_con.Dispose();
            _con.Close();
        }
    }
}