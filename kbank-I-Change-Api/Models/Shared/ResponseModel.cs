﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace kbank_I_Change_Api.Models.Shared
{
    public class ResponseModel
    {
        public bool Successful { get; set; }
        public int? Type { get; set; } //1 = Seccess; 2 = Error; 3 = Warning
        public string Information { get; set; }
        public object Result { get; set; }
    }
}