﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Web;

namespace kbank_I_Change_Api.Models.Shared
{
    public class UserDataModel
    {
        public string UserID { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserCode { get; set; }
        public string Role { get; set; }
        public string PermissionData { get; set; }
        public string UserMenuData { get; set; }
        public string Guid { get; set; }

        public UserDataModel()
        {

        }

        public UserDataModel(JwtSecurityToken data)
        {
            UserID = data.Claims.First(claim => claim.Type == "UserID").Value;
            UserName = data.Claims.First(claim => claim.Type == "UserName").Value;
            FirstName = data.Claims.First(claim => claim.Type == "FirstName").Value;
            LastName = data.Claims.First(claim => claim.Type == "LastName").Value;
            UserCode = data.Claims.First(claim => claim.Type == "UserCode").Value;
            Role = data.Claims.First(claim => claim.Type == "Role").Value;
            PermissionData = data.Claims.First(claim => claim.Type == "PermissionData").Value;
            UserMenuData = data.Claims.First(claim => claim.Type == "UserMenuData").Value;
            Guid = data.Claims.First(claim => claim.Type == "jti").Value;
        }
    }
}