﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace kbank_I_Change_Api.Models.Login
{
    public class UserPermissionModel
    {
        public decimal ROLE_PAGE_ID { get; set; }
        public string ROLE_PAGE_CODE { get; set; }
        public string ROLE_CODE { get; set; }
        public string PAGE_CODE { get; set; }
        public string PERMISSION { get; set; }
        public string PERMISSION_NAME { get; set; }
    }
}