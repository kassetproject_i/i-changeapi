﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace kbank_I_Change_Api.Models.DB
{
    public class ICNG_M_SEC_PAGE
    {
        public decimal PAGE_ID { get; set; }
        public string PAGE_CODE { get; set; }
        public string PAGE_NAME { get; set; }
        public string PAGE_LEVEL { get; set; }
        public string PARENT_PAGE { get; set; }
        public string URL { get; set; }
        public decimal SEQ_NO { get; set; }
        public string REMARK { get; set; }
        public string STATUS { get; set; }
        public string CREATE_USER_ID { get; set; }
        public DateTime CREATE_DATETIME { get; set; }
        public string UPDATE_USER_ID { get; set; }
        public DateTime UPDATE_DATETIME { get; set; }
        public decimal SEQ_NO_ROLEPAGE { get; set; }
        public string IS_DISPLAY { get; set; }
    }
}