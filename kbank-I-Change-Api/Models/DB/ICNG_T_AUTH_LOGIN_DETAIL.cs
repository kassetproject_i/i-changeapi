﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace kbank_I_Change_Api.Models.DB
{
    public class ICNG_T_AUTH_LOGIN_DETAIL
    {
        public decimal AUTH_LOGIN_DETAIL_ID { get; set; }
        public decimal AUTH_LOGIN_ID { get; set; }
        public DateTime EXPIRE_DATE { get; set; }
        public string CREATE_USER_ID { get; set; }
        public DateTime CREATE_DATETIME { get; set; }
        public string UPDATE_USER_ID { get; set; }
        public DateTime UPDATE_DATETIME { get; set; }
    }
}