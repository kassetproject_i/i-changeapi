﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace kbank_I_Change_Api.Models.DB
{
    public class ICNG_M_SEC_ROLE
    {
        public decimal ROLE_ID { get; set; }
        public string ROLE_CODE { get; set; }
        public string ROLE_NAME { get; set; }
        public string ROLE_PAGE_CODE_DEFAULT { get; set; }
        public string STATUS { get; set; }
        public string CREATE_USER_ID { get; set; }
        public DateTime CREATE_DATETIME { get; set; }
        public string UPDATE_USER_ID { get; set; }
        public DateTime UPDATE_DATETIME { get; set; }
    }
}