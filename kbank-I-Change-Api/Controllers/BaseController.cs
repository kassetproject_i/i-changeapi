﻿using kbank_I_Change_Api.BLL;
using kbank_I_Change_Api.Models.Shared;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace kbank_I_Change_Api.Controllers
{
    public class BaseController : ApiController
    {
        public ResponseModel _responseData;
        private JwtTokensService _jwtTokensService;
        public UserDataModel userData;

        public BaseController()
        {
            _responseData = new ResponseModel();
            _jwtTokensService = new JwtTokensService();
            userData = new UserDataModel();
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");
        }

        protected override void Initialize(HttpControllerContext controllerContext)
        {
            if (controllerContext.Request.Headers.Authorization != null)
            {
                string token = controllerContext.Request.Headers.Authorization.Parameter;
                JwtSecurityToken res = _jwtTokensService.GetTokenData(token);
                userData = new UserDataModel(res);
            }

            base.Initialize(controllerContext);
        }
    }
}
