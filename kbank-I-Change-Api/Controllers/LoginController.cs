﻿using kbank_I_Change_Api.BLL;
using kbank_I_Change_Api.Models.Login;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace kbank_I_Change_Api.Controllers
{
    public class LoginController : BaseController
    {
        LoginService _loginService;

        public LoginController()
        {
            _loginService = new LoginService();
        }

        [HttpPost]
        public HttpResponseMessage Login(UserLoginModel userLogin)
        {
            _responseData = _loginService.Login(userLogin);
            return Request.CreateResponse(HttpStatusCode.OK, _responseData);
        }

        [BasicAuthentication]
        [HttpGet]
        public HttpResponseMessage Logout()
        {
            _responseData = _loginService.Logout(userData);
            return Request.CreateResponse(HttpStatusCode.OK, _responseData);
        }

        [BasicAuthentication]
        [HttpGet]
        public HttpResponseMessage GetTestData()
        {
            var userDataValue = userData;
            return Request.CreateResponse(HttpStatusCode.OK, "5555555");
        }
    }
}